async function getOutboundMessages(to, from) {
  try {
    const messages = await strapi.services.message.find({ to: to, from: from });
    return messages;
  } catch (error) {
    console.log('Error retrieving outbound messages', error);
  }
}
async function getInboundMessages(to, from) {
  try {
    return await strapi.services.message.find({ from: from, to: to });
  } catch (error) {
    console.log('Error retrieving outbound messages', error);
  }
}

async function getAndCombineMessages(from, to) {
  let outboundMessages = await getOutboundMessages(from, to);
  let inboundMessages = await getInboundMessages(to, from);

  if (outboundMessages.length && inboundMessages.length)
    return inboundMessages
      .concat(outboundMessages)
      .sort((a, b) => a.published_at - b.published_at);
  else if (outboundMessages.length && inboundMessages.length == 0)
    return outboundMessages;
  else if (outboundMessages.length == 0 && inboundMessages.length)
    return inboundMessages;
  else return [];
}

async function saveMessage(message) {
  try {
    return await strapi.services.message.create(message);
  } catch (error) {
    console.log('Could not add message', error);
  }
}

module.exports = {
  getAndCombineMessages,
  saveMessage,
  //TODO save user.socketIoId for a user presence feature
  //TODO delete user.socketIoId for a user presence feature
};
