'use strict';

/**
 * An asynchronous bootstrap function that runs before
 * your application gets started.
 *
 * This gives you an opportunity to set up your data model,
 * run jobs, or perform some special logic.
 *
 * See more details here: https://strapi.io/documentation/developer-docs/latest/setup-deployment-guides/configurations.html#bootstrap
 */

require('dotenv').config();

const { getAndCombineMessages, saveMessage } = require('./util/database');

module.exports = () => {
  var io = require('socket.io')(strapi.server, {
    cors: {
      origin: process.env.ALLOWED_CORS_ORIGINS
        ? process.env.ALLOWED_CORS_ORIGINS.split(',')
        : [],
      methods: ['GET', 'POST'],
      allowedHeaders: ['my-custom-header'],
      credentials: true,
    },
  });
  io.on('connection', function (socket) {
    socket.on('join', async ({ me, contact, room }) => {
      socket.join(room);

      const messages = await getAndCombineMessages(me, contact);
      socket.emit('get message history', messages);
    });
    socket.on('push new message', async (message, room) => {
      saveMessage(message);
      socket.to(room).emit('new message', message);
    });
  });

  //TODO user presence feature
  // save user socket.io id into db
  // get counterpart user (based on the contactId) socket.io id from db
  // emit user presence to counterpart (and back to the connected user)

  //TODO socket.io on disconnect handler for a user presence feature
  // - delete user socket.io id from db
  // - emit presence to counterpart
};
