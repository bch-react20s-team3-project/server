module.exports = ({ env }) => ({
  host: env('HOST', '0.0.0.0'),
  port: env.int('PORT', 1337),
  admin: {
    auth: {
      secret: env('ADMIN_JWT_SECRET', '67870b840a7b91bed646550d8c697283'),
    },
    watchIgnoreFiles: ['**/config-sync/files/**'],
  },
});
