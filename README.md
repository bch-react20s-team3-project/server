# Jobvious server
## Development
To set up your development environment, please do the following:
- `npm install` to install all the required dependencies
- `npx husky install` to install local husky dependencies
- `npm run develop` to start the app in development mode
  - Note that there's also a ready made vscode launch configuration for the same. You can just press F5 to start the server in debug mode.

Please also take not of the most important env variables for connecting to a database, making frontend redeployments on data updates, and for the allowed CORS origins:
```
# Example of local config for development with local Gatsby and MongoDB:
DATABASE_NAME=jobvious_db
DATABASE_URI=mongodb://localhost:27017/jobvious_db?readPreference=primary&ssl=false
WEB_DEPLOY_HOOK_URL==http://localhost:8000/__refresh
ALLOWED_CORS_ORIGINS=http://localhost:8000

# Using MongoDB Atlas will require the more detailed configs:
DATABASE_HOST=...
DATABASE_SRV=true
DATABASE_USERNAME=...
DATABASE_PASSWORD=...
DATABASE_SSL=true
DATABASE_NAME=...
```

Please refer to our main [environment markdown](https://gitlab.com/bch-react20s-team3-project/documentation/-/blob/dev/ENVIRONMENTS.md) for further details.

## Testing
Any GraphQL client will do, e.g. a Chrome extension like [Altair GraphQL Client](https://chrome.google.com/webstore/detail/altair-graphql-client/flnheeellpciglgpaodhkhmapeljopja).

Of course, our Strapi configuration generates both GraphQL as well as REST APIs, so any REST client can be used too. Note that the API (and roles/permissions) design has been done with GraphQL in mind.
