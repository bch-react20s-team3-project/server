'use strict';

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#lifecycle-hooks)
 * to customize this model
 */

module.exports = {
  lifecycles: {
    async beforeCreate(data) {
      data.filtered = await getAllPositionProfileIds();
    },
    async beforeUpdate(params, data) {
      if (data.liked) updateCounterpartMatches(data, 'liked');
      if (data.matched) updateCounterpartMatches(data, 'matched');
    },
  },
};

/**
 * Gets all position profile ids that can be suggested as a list of filtered matches
 * @returns {string[]} Array of position-profile object ids
 */
async function getAllPositionProfileIds() {
  try {
    const allPositions = await strapi.query('position-profile').find({});
    return allPositions.map(p => p.id);
  } catch (error) {
    console.log('Error populating the new position match list');
  }
}

/**
 * Updates talent-match objects based on the updates in the data of the update to this position-match object
 * @param {*} data Update data for this position-match object update
 * @param {"liked"|"matched"} source Source list of updated talent-profile ids to check
 */
async function updateCounterpartMatches(data, source) {
  // Get the talent profile id of the user who owns the position matches to be updated
  const {
    profile: [
      {
        profile: { id: userProfileId },
      },
    ],
  } = await strapi
    .query('user', 'users-permissions')
    .findOne({ id: data.owner });

  // Get the ids of the owners of the positions liked by the current user
  const counterpartUserIds = (
    await strapi.query('position-profile').find({ id_in: data[source] })
  ).map(position => position.owner.owners);

  // For each counterpart user...
  counterpartUserIds.forEach(async counterpart => {
    // ...check their likes...
    const counterpartMatches = (
      await strapi
        .query('user', 'users-permissions')
        .findOne({ id: counterpart })
    ).matches[0].matches[0];

    // ...if they've liked the current talent.
    if (
      counterpartMatches.liked
        .map(likedId => String(likedId))
        .includes(String(userProfileId))
    ) {
      // Update the counterpart talent matches by moving the corresponding id from likes to matches
      strapi.query('talent-match').update(
        { id: counterpartMatches._id },
        {
          liked: counterpartMatches.liked.filter(
            likedId => String(likedId) !== userProfileId
          ),
          matched: counterpartMatches.matched.concat(userProfileId),
          owner: counterpartMatches.owner,
        }
      );
    }
  });
}

//TODO @Janne #blocking-contacts if user adds something into .ignored, update counterpart data also if needed (matched -> liked)
