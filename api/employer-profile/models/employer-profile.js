'use strict';

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#lifecycle-hooks)
 * to customize this model
 */

require('dotenv').config();
const axios = require('axios').default;

module.exports = {
  lifecycles: {
    async afterCreate() {
      redeployWebUi();
    },
    async afterUpdate() {
      redeployWebUi();
    },
  },
};

async function redeployWebUi() {
  if (process.env.WEB_DEPLOY_HOOK_URL)
    if (process.env.WEB_DEPLOY_HOOK_TOKEN)
      axios.post(process.env.WEB_DEPLOY_HOOK_URL, undefined, {
        headers: {
          authorization: process.env.WEB_DEPLOY_HOOK_TOKEN,
        },
      });
    else axios.post(process.env.WEB_DEPLOY_HOOK_URL);
}
