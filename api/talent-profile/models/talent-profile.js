'use strict';

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#lifecycle-hooks)
 * to customize this model
 */

require('dotenv').config();
const axios = require('axios').default;

module.exports = {
  lifecycles: {
    async afterCreate() {
      redeployWebUi();
    },
    async afterUpdate(result) {
      redeployWebUi();
      await updateMatches(result.id);
    },
  },
};

/**
 * Adds the id of the updated profile to the list of filtered talent matches of all users
 * @param {string} id Id of the profile object that was updated
 * @returns {void} void
 */
async function updateMatches(id) {
  try {
    // Get all talent match instances
    const allMatchLists = await strapi.query('talent-match').find({});

    // Update each...
    allMatchLists.forEach(current => {
      // ...by adding the id given as param to the list of filtered ids (if it's not already in the current filtered/liked/matched list)
      if (
        !current.filtered.map(f => f.id).includes(id) &&
        !current.liked.map(l => l.id).includes(id) &&
        !current.matched.map(m => m.id).includes(id)
      ) {
        strapi.query('talent-match').update(
          { id: current.id },
          {
            // If there's other ids already, spread and add the id of the updated profile, otherwise just add the updated one
            filtered: current.filtered.length
              ? [...current.filtered.map(f => f.id), id]
              : [id],
          }
        );
      }
    });
  } catch (error) {
    console.log(
      'Error updating match lists based on an updated profile',
      error
    );
  }
}

async function redeployWebUi() {
  if (process.env.WEB_DEPLOY_HOOK_URL)
    if (process.env.WEB_DEPLOY_HOOK_TOKEN)
      axios.post(process.env.WEB_DEPLOY_HOOK_URL, undefined, {
        headers: {
          authorization: process.env.WEB_DEPLOY_HOOK_TOKEN,
        },
      });
    else axios.post(process.env.WEB_DEPLOY_HOOK_URL);
}
