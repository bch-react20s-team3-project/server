'use strict';

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#lifecycle-hooks)
 * to customize this model
 */

module.exports = {
  lifecycles: {
    async beforeCreate(data) {
      data.filtered = await getAllTalentProfileIds();
    },
    async beforeUpdate(params, data) {
      if (data.liked) updateCounterpartMatches(data, 'liked');
      if (data.matched) updateCounterpartMatches(data, 'matched');
    },
  },
};

/**
 * Gets all talent profile ids that can be suggested as a list of filtered matches
 * @returns {string[]} Array of talent-profile object ids
 */
async function getAllTalentProfileIds() {
  try {
    const allTalents = await strapi
      .query('talent-profile')
      .find({ draft: false });
    return allTalents.map(t => t.id);
  } catch (error) {
    console.log('Error populating the new position match list');
  }
}

/**
 * Updates position-match objects based on the updates in the data of the update to this talent-match object
 * @param {*} data Update data for this talent-match object update
 * @param {"liked"|"matched"} source Source list of updated position-profile ids to check
 */
async function updateCounterpartMatches(data, source) {
  // Get the employer profile id of the user who owns the talent matches to be updated
  const {
    profile: [
      {
        profile: { id: userProfileId },
      },
    ],
  } = await strapi
    .query('user', 'users-permissions')
    .findOne({ id: data.owner });

  // Get the ids of positions owned by the current user
  const userPositionIds = (
    await strapi.query('employer-profile').findOne({ id: userProfileId })
  ).positions.map(position => position.id);

  // Get the ids of talents liked by the current user and the ids of the positions liked by them
  const counterpartLikes = (
    await strapi.query('talent-profile').find({ id_in: data[source] })
  ).map(talent => ({
    talentId: talent.id,
    matchesId: talent.owner.matches[0].matches.id,
    liked: talent.owner.matches[0].matches.liked,
    matched: talent.owner.matches[0].matches.matched,
    owner: talent.owner.id
  }));

  // For each position of the current user...
  userPositionIds.forEach(currentPositionId => {
    // ...check the likes of each potential match counterpart...
    counterpartLikes.forEach(async counterpart => {
      // ...if they've liked the position.
      if (
        counterpart.liked
          .map(likedId => String(likedId))
          .includes(currentPositionId)
      ) {
        // Update the counterpart position matches by moving the corresponding id from likes to matches
        strapi.query('position-match').update(
          { id: counterpart.matchesId },
          {
            liked: counterpart.liked.filter(
              likedId => String(likedId) !== currentPositionId
            ),
            matched: counterpart.matched.concat(currentPositionId),
            owner: counterpart.owner
          }
        );
      }
    });
  });
}

//TODO @Janne #blocking-contacts if user adds something into .ignored, update counterpart data also if needed (matched -> liked)
