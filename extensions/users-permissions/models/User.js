require('dotenv').config();
const axios = require('axios').default;

module.exports = {
  lifecycles: {
    async beforeCreate(user) {
      // Find and assign user role id as per desiredRole in the request
      user.role = (
        await strapi
          .query('role', 'users-permissions')
          .findOne({ name: user.desiredRole })
      ).id;
    },
    async afterUpdate() {
      redeployWebUi();
    },
  },
};

async function redeployWebUi() {
  if (process.env.WEB_DEPLOY_HOOK_URL)
    if (process.env.WEB_DEPLOY_HOOK_TOKEN)
      axios.post(process.env.WEB_DEPLOY_HOOK_URL, undefined, {
        headers: {
          authorization: process.env.WEB_DEPLOY_HOOK_TOKEN,
        },
      });
    else axios.post(process.env.WEB_DEPLOY_HOOK_URL);
}
